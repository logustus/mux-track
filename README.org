#+title:     mux-track
#+author:    Logan Barnett-Hoy
#+email:     logustus@gmail.com
#+date:      <2020-09-29 Tue>
#+language:  en
#+file_tags: mux-track
#+tags:

* mux-track

=mux-track= multiplexes audio tracks in parallel. Use it to seamlessly
transition from one track to another, all while keeping the audio in play-back
sync.

** why?

I run a D&D campaign, and I like to run a curated list of ambient music. One of
the video games that I use the music from is Divinity of Sin 2, which has some
absolutely wonderful music. In the game one can assign an instrument to each of
their characters. When that player's turn comes up it plays that character's
instrument within the current music.

With =mux-track= I can have setup an initiative changing hook to switch to that
player's instrument at my table. If that doesn't sound sweet to you, it means
you are a robot with no soul.

** heavy development

This program is still immature. For example, the songs it plays are hardcoded,
yet not provided to respect copyright. Here is a list of issues needing to be
resolved before we could consider this "one-oh":

+ Make audio tracks loaded configurable.
+ Make HTTP port configurable.
+ Provide some copyright friendly audio tracks to make contribution easier.
+ Provide audio or video that demonstrates the capabilities of =mux-track=.
+ Use high fidelity logging.
+ Build and publish a release.

** running

From the shell invoke:

#+begin_example shell
cargo run --release
#+end_example
** switching tracks

By default the HTTP server runs on 3704. Send an HTTP =POST= to =/track/switch=
with the track name as a JSON string.

For example:

#+begin_src shell
curl \
  -X POST \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d '"foobar.mp3"' \
  'http://localhost:3704/track/switch' \
#+end_src

Also invoking =switch-track.sh= with an instrument name will switch between one
of the hard coded tracks that didn't ship with this repository. Yeah I know it's
dumb.

** crackling audio on debug builds

This is a known [[https://github.com/RustAudio/rodio/issues/225][issue with rodio]]. Folks have been looking into it and trying
things out. Stay tuned! Or, you know, chip in :)
