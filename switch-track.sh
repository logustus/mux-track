#! /usr/bin/env bash

set -x

song=${1:-"Cello"}
curl \
  -X POST \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d '"'"Symphony Of The Void - $song.mp3"'"' \
  'http://localhost:3704/track/switch' \