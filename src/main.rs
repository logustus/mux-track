/**
 * Copyright 2020 Logan Barnett <logustus@gmail.com>.
 *
 * See LICENSE.txt for full MIT license.
 *
 * This program comprises the core of the track multiplexer. Here, audio tracks
 * are kept in sync and mixed together. When multiplexing to another track, the
 * audio does a smooth transition between them. If the tracks are designed well
 * for this kind of multiplexing, it should be very seamless. A well designed
 * audio track is the same duration of the other tracks, and shares some musical
 * properties (I don't know the names) which allow for parallel playback.
 *
 * A notable example of this is from the Divinity II: Original Sin game
 * soundtrack. There are many songs that use a character-specific instrument,
 * but they are all variants of the same song. These songs can be found suffixed
 * with bansuri, cello, oud, and tambura for songs such as Symphony of the Void
 * on YouTube. It is left as an exercise of the reader to find living examples.
 *
 * The design is such that requests come in from a web service endpoint that
 * indicate a track to switch to. This will post an instruction to an actor
 * queue (perhaps a channel, in this case). The worker thread will slowly lower
 * the volume of the source track, and increase the volume of the destination
 * track. All the meanwhile the tracks are playing in sync, any non-active track
 * has its volume at or trending to zero.
 */

use actix_web::{
    App,
    HttpResponse,
    HttpServer,
    Responder,
    error,
    post,
    web,
};
use bytes::{BytesMut};
use crossbeam_channel::{bounded, Sender, Receiver};
use futures::StreamExt;
use rodio::Device;
use rodio::Sink;
use rodio::Source;
use std::fs::File;
use std::io::BufReader;
use std::time::{Duration, Instant};

// Statics with any sort of logic in them simply don't work yet in Rust, but
// hey, functions are more composable anyways, right?
fn dummy_track_name() -> TrackName {
    TrackName(String::from("dummy-track"))
}

// Statics with any sort of logic in them simply don't work yet in Rust, but
// hey, functions are more composable anyways, right?
//
// TODO: Use impl to create a Track::default() or Track::dummy().
fn dummy_track() -> Track {
    let device = rodio::default_output_device().unwrap();
    let source = rodio::source::SineWave::new(440);
    let sink = Sink::new(&device);
    sink.append(source);
    sink.set_volume(0.0);
    Track { name: dummy_track_name(), sink }
}

/**
 * This holds the Sink with its identifier. As such this is the core element of
 * the application, but nobody gets to own but the root level processes. If you
 * need a track, you can refer to it via its name.
 */
struct Track {
    name: TrackName,
    sink: Sink,
}

/**
 * Even Strings can be truly type safe. Use TrackName to refer to a Track, and
 * nothing else.
 */
#[derive(Clone, Eq)]
struct TrackName(String);
impl PartialEq for TrackName {
    fn eq(&self, other: &Self) -> bool {
      String::eq(&self.0, &other.0)
    }
}

struct VolumeMux {
    from: VolumeTransition,
    to: VolumeTransition,
}

// Initially my design for VolumeTransition included the Sink to operate upon.
// This is rather imperative thinking. Yet thinking I initially felt like Rust
// supported, nay, encouraged. I ran into lots of restrictions with the borrow
// checker, mutability, and references.
//
// Digressing: Answers I find on Stack Overflow, and the Rust books/docs
// themselves all feel woefully apologetic/inadequate at explaining the need for
// the borrow checking and the limitations that come with it. I like guards. I
// like zero runtime cost guards a lot more. But why do I feel like, if
// anything, things have become more dangerous? Perhaps the real answer is that
// the borrow checker is there when I need it, and not something I should lean
// upon.
#[derive(Clone)]
struct VolumeTransition {
    desired_volume: f32,
    name: TrackName,
    start_time: Instant,
    transitioning: bool,
}

fn load_track(device: &Device, name: &str) -> Track {
    let file = File::open(["media/", name].concat()).unwrap();
    let source = rodio::Decoder::new(BufReader::new(file)).unwrap();
    let repeat = source.buffered().repeat_infinite();
    let sink = Sink::new(&device);
    sink.append(repeat);
    sink.set_volume(0.0);
    Track { name: TrackName(String::from(name)), sink }
}


fn track_names_to_mux(from: TrackName, to: TrackName) -> VolumeMux {
    VolumeMux {
        from: decrease(from),
        to: increase(to),
    }
}

fn transition_mux(ts: &Vec<Track>, m: &mut VolumeMux) -> () {
    let dummy = &dummy_track();
    let from_sink = &find_track_by_name(&ts, m.from.name.clone())
        .unwrap_or(dummy).sink;
    transition_volume(&mut m.from, from_sink);
    let to_sink = &find_track_by_name(&ts, m.to.name.clone())
        .unwrap_or(dummy).sink;
    transition_volume(&mut m.to, to_sink);
}

// TODO: It would be much easier to reason about if this were expressed in
// seconds needed make a full transition.
const SPEED: f32 = 0.0002;
fn transition_volume(mut transition: &mut VolumeTransition, sink: &Sink) -> () {
    if transition.transitioning {
        let elapsed = transition.start_time.elapsed().as_millis() as f32;
        let computed = elapsed * SPEED * if transition.desired_volume == 1.0 {
            1.0
        } else {
            -1.0
        } + if transition.desired_volume == 1.0 { 0.0 } else { 1.0 };
        let volume = if computed >= 1.0 && transition.desired_volume == 1.0 {
            transition.transitioning = false;
            println!(
                "Done setting {} to 1, took {}s.",
                transition.name.0,
                transition.start_time.elapsed().as_secs(),
            );
            1.0
        } else if computed <= 0.0 && transition.desired_volume == 0.0 {
            transition.transitioning = false;
            println!(
                "Done setting {} to 0, took {}s.",
                transition.name.0,
                transition.start_time.elapsed().as_secs(),
            );
            0.0
        } else {
            computed
        };
        sink.set_volume(volume);
    }
}

fn muxing(m: &VolumeMux) -> bool {
  m.from.transitioning || m.to.transitioning
}

fn volume_queue(
    r: Receiver<TrackName>,
    ts: Vec<Track>,
) {
    // return std::thread::scope(move || {
    std::thread::spawn(move || {
        loop {
            println!("Waiting for track mux message...");
            let to_name = r.recv().unwrap();
            println!("Got a track mux message...");
            let from_name = find_current_track_name(&ts);
            if from_name == to_name  {
                println!(
                    "Track name {} is already playing. Skipping.",
                    from_name.0,
                );
            } else {
                let mut mux = track_names_to_mux(from_name, to_name);
                while muxing(&mux) {
                    transition_mux(&ts, &mut mux);
                    std::thread::sleep(Duration::from_millis(5));
                }
            }
        }
    });
}

fn find_track_by_name<'a>(ts: &'a Vec<Track>, name: TrackName) -> Option<&'a Track> {
    ts
        .iter()
        .filter(|x| x.name == name)
        .nth(0)
}

fn find_current_track_name(ts: &Vec<Track>) -> TrackName {
    ts
        .iter()
        .filter(|x| x.sink.volume() == 1.0)
        .nth(0)
        .unwrap_or(&dummy_track())
        .name
        .clone()
}

fn transition_from_track(
    desired_volume: f32,
    name: TrackName,
) -> VolumeTransition {
    VolumeTransition {
        desired_volume,
        name: name,
        start_time: Instant::now(),
        transitioning: true,
    }
}
// Partially applied.
fn decrease(t: TrackName) -> VolumeTransition { transition_from_track(0.0, t) }
fn increase(t: TrackName) -> VolumeTransition { transition_from_track(1.0, t) }

const MAX_PAYLOAD_SIZE: usize = 2 * 1024; // Max payload size is 2k.
#[post("/track/switch")]
async fn post_switch_track(
    mut payload: web::Payload,
    app_data: web::Data<Sender<TrackName>>,
) -> impl Responder {
    println!("Received request to mux song!");
    let mut body = BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk?;
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > MAX_PAYLOAD_SIZE {
            return Err(error::ErrorBadRequest("overflow"));
        }
        body.extend_from_slice(&chunk);
    }
    // body is loaded, now we can deserialize serde-json
    println!("Deserializing candidate song... {:?}", body);
    let song_name: String = serde_json::from_slice::<String>(&body)?;
    println!("Got request to switch to song {}", song_name);
    app_data.send(TrackName(song_name.clone())).unwrap();
    println!("Sent song mux message for {}", song_name);
    // HttpResponse::Ok().finish()
    Ok(HttpResponse::Ok())
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    // TODO: Make these as configurable. They won't be bundled with the repo
    // anyways.
    let songs = [
        // These all share the same playback length and overlap nicely.
        "Symphony Of The Void - Bansuri.mp3",
        "Symphony Of The Void - Cello.mp3",
        "Symphony Of The Void - Oud.mp3",
        "Symphony Of The Void - Tambura.mp3",
        // Not an official track - this was chopped up from the original
        // Symphony of the Void.
        "Symphony Of The Void - Trumpet.mp3",
    ];
    let device = rodio::default_output_device().unwrap();
    let tracks: Vec<_> = songs.into_iter()
        .map(|x| load_track(&device, x))
        .collect();
    // Maybe we should consider tracks to be "valid" tracks.
    // tracks.push(dummy_track(&device));
    for t in &tracks { t.sink.play(); }
    let (s, r) = bounded(1);
    println!("volume_queue");
    volume_queue(r, tracks);
    HttpServer::new(move || {
        println!("Starting HttpServer...");
        App::new()
            .data(s.clone())
            .service(post_switch_track)
    })
        .bind("127.0.0.1:3704")?
        .run()
        .await
}
